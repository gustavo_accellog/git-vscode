# GIT + VSCODE + SSH KEY

## Objetivo

Como utilizar uma chave SSH para fazer commits com GIT (Bitbucket) e utilizar o VS Code para fazer os commits e push para o servidor.

## Passos

* [Gerar a chave SSH](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html#SetupanSSHkey-ssh2) no computador local (testei no Linux) ```$ ssh-keygen```
* Em configurações do Bitbucket, clicar em Chaves SSH e adicionar uma chave. Copiar chave pública e dar um nome para a chave (o Link anterior mostra todo este processo).
* Clonar o repositório do Bitbucket usando a chave ssh e não a https.
* Abrir o VSCode e fazer alterações no projeto.
* Clicar em Source Control para verificar as alterações feitas.
* Adicionar todas as modificações ao GIT ou arquivo por arquivo (+ / Stage Changes).
* No input de mensagem, informe seu comentário e pressione Ctrl+Enter.
* Nos três pontos acima (source control), clique em "push".

## Observação

Este é apenas um projeto de teste.
Crie a chave sem um senha para funcionar aqui no VSCode. Não consegui fazer funcionar com senha.